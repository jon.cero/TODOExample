//
//  TODOItemManager.swift
//  TODOExample
//
//  Created by PixelByte on 14/09/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

import Foundation

// MARK: - Singleton
class TODOItemManager: NSObject {
    
    private var TODOItems = [TODOItem]()
    
    // MARK: - Shared Instance
    static let sharedInstance: TODOItemManager = {
        let instance = TODOItemManager()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        
        do {
    
            // Try to load from persistence
            TODOItems = try [TODOItem].readFromPersistence()
            
        } catch let error as NSError {
            
            if error.domain == NSCocoaErrorDomain && error.code == NSFileReadNoSuchFileError {
                NSLog("No persistence file found")
                
            } else {
                NSLog("Error loading from persistence: \(error)")
            }
        }
        
        super.init()
    }
    
//    private func updateTODOItemList(item:TODOItem) {
//        
//    }
    
    func getPendingTODOItem() -> Array<TODOItem> {
        
        var pendingTODOItems = [TODOItem]()
        
        for item in TODOItems {
            if !item.done {
                pendingTODOItems.append(item)
            }
        }
        
        NSLog("getPendingTODOItem raw: %d, sorted: %d", TODOItems.count, pendingTODOItems.count)
        
        return pendingTODOItems
    }
    
    func getCompletedTODOItem() -> Array<TODOItem> {
        
        var completedTODOItems = [TODOItem]()
        
        for item in TODOItems {
            if item.done {
                completedTODOItems.append(item)
            }
        }
        
        NSLog("getCompletedTODOItem raw: %d, sorted: %d", TODOItems.count, completedTODOItems.count)
        
        return completedTODOItems
    }
    
    func addTODOItem(item:TODOItem) {
        
        NSLog("addTODOItem raw count %d", TODOItems.count)
        
        self.TODOItems.append(item)
        
        NSLog("item title: %@, item done: %@, total item count: %d", item.title, item.done.description, TODOItems.count)
        
        do {
            try TODOItems.writeToPersistence()
        } catch let error {
            NSLog("Error writing to persistence: \(error)")
        }
        
    }
    
    func updateTODOItem(item:TODOItem) {
        
        NSLog("updateTODOItem raw count %d", TODOItems.count)
        
        let indexOfA = TODOItems.index(of: item)
        
        if !TODOItems[indexOfA!].done {
            TODOItems[indexOfA!].title = item.title + "(" + dateTime() + ")"
        } else {
            let breakTitle = TODOItems[indexOfA!].title.components(separatedBy: "(")
            TODOItems[indexOfA!].title = breakTitle[0]
        }

        TODOItems[indexOfA!].done = !item.done
        
        NSLog("item title: %@, item done: %@", TODOItems[indexOfA!].title, TODOItems[indexOfA!].done.description)
        
        do {
            try TODOItems.writeToPersistence()
        } catch let error {
            NSLog("Error writing to persistence: \(error)")
        }
        
    }
    
    func removeTODOItem(item:TODOItem) {
        
        NSLog("removeTODOItem raw count %d", TODOItems.count)
        
        let indexOfA = TODOItems.index(of: item)
        
        NSLog("item title: %@, item done: %@, total item count: %d", TODOItems[indexOfA!].title, TODOItems[indexOfA!].done.description, TODOItems.count)
        
        TODOItems.remove(at: indexOfA!)
        
        do {
            try TODOItems.writeToPersistence()
        } catch let error {
            NSLog("Error writing to persistence: \(error)")
        }
        
    }
    
    func dateTime() -> String {
        
        //source: https://stackoverflow.com/questions/24070450/how-to-get-the-current-time-as-datetime
        
        // get the current date and time
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .short
        
        // get the date time String from the date object
        return formatter.string(from: currentDateTime) // October 8, 2016 at 10:48:53 PM
    }
}
