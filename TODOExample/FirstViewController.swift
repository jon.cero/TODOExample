//
//  FirstViewController.swift
//  TODOExample
//
//  Created by PixelByte on 14/09/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: - interface
    @IBOutlet var pendingTableView: UITableView!
    
    // MARK: - variables
    private var TODOItems = [TODOItem]()
    
    // MARK: - VC controls
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pendingTableView.delegate = self
        self.pendingTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        TODOItems = TODOItemManager.sharedInstance.getPendingTODOItem()
        pendingTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - tableview delegation
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TODOItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pendingCell", for: indexPath)
        
        if indexPath.row < TODOItems.count {
            
            let item = TODOItems[indexPath.row]
            
            cell.textLabel?.text = item.title
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row < TODOItems.count {
            
            let item = TODOItems[indexPath.row]
            
            TODOItemManager.sharedInstance.updateTODOItem(item: item)
            
            TODOItems.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .top)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if indexPath.row < TODOItems.count {
            
            let alert = UIAlertController (
                title: "Remove item?",
                message: "Are you sure to delete the item?",
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                
                let item = self.TODOItems[indexPath.row]
                
                TODOItemManager.sharedInstance.removeTODOItem(item: item)
                
                self.TODOItems.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .top)
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - add button
    @IBAction func addItemBtnC(_ sender: Any) {
        
        let alert = UIAlertController(
            title: "Add item",
            message: "Insert name of TODO item: ",
            preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: nil)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            
            if let title = alert.textFields?[0].text, title.characters.count > 0 {
                self.addNewToDoItem(title: title)
            }
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func addNewToDoItem(title: String) {
        
        let newTODOItem = TODOItem(title: title)
        
        let pendingIndex = self.TODOItems.count
        TODOItemManager.sharedInstance.addTODOItem(item: newTODOItem)
        self.TODOItems.append(newTODOItem)
        self.pendingTableView.insertRows(at: [IndexPath(row: pendingIndex, section: 0)], with: .top)

    }
}

